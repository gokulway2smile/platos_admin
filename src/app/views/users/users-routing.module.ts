import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { UsersComponents } from './users.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponents,
    data: {
      title: 'Users'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
