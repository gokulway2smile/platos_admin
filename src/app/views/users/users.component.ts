import {
    Component,
    OnInit,
    ViewChild,
    TemplateRef
} from '@angular/core';
import {
    FormControl,
    FormGroup,
    FormBuilder,
    Validators
} from '@angular/forms';
import {
    HttpClient
} from '@angular/common/http';

import {
    ActivatedRoute,
    Router
} from "@angular/router";
import {
    DatatableComponent
} from '@swimlane/ngx-datatable';
import {
    AppSettings
} from '../../app.setting';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'users.component.html'
})
	


export class UsersComponents implements OnInit { 

 constructor( private router: Router, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) {
    }

    rowsFilter:any;
    AllData:any;
    FirstName:any;
    SurName:any;
    EmailAddress:any;
    MobileNo:any;
    Password:any;
    DeliveryPincode:any;
    LeadTime:any;
    DeliveryTime:any
    error:any;
    MinOrder:any;
    maxOrder:any;
    EventType:any;
    GSTCertificate:any;
    FoodCertificate:any;
    Scores:any;
    FileUpload:any;
    showform:boolean= false;


     ngOnInit() {
     	this.loaddata();

     }

     close()
     {
       this.showform = false;
     }

     addAction()
     {
       this.showform =true;
        this.FirstName="";
        this.SurName="";
        this.EmailAddress="";
        this.MobileNo="";
        this.Password="";
        this.DeliveryPincode="";
        this.LeadTime="";
        this.DeliveryTime="";
        this.MinOrder="";
        this.maxOrder="";
        this.EventType="";
        this.GSTCertificate="";
        this.FoodCertificate="";
        this.Scores="";
        this.FileUpload="";

     }

     onSubmit()
     {
        
            let data = {
            "FirstName" : this.FirstName,
            "SurName": this.SurName,
            "EmailAddress": this.EmailAddress,
            "MobileNo": this.MobileNo,
            "Password" : this.Password,
            "DeliveryPincode":this.DeliveryPincode,
            "LeadTime": this.LeadTime,
            "DeliveryTime" : this.DeliveryTime,            
            "MinOrder" : this.MinOrder,
            "maxOrder" : this.maxOrder,
            "EventType" : this.EventType,
            "GSTCertificate" : this.GSTCertificate,
            "FoodCertificate" :this.FoodCertificate,
            "Scores" :this.Scores,
            "FileUpload":this.FileUpload
            }


           this.http.post(AppSettings.API_ENDPOINT + 'Catter_NewAdmin/', {
                "data": data,
            }).subscribe((data: any) => {

                console.log(data);
                if (data.status == "not_ok") {
                    this.error = data.data;
                    return;
                } else {
                
                Swal("Success", "Caterer Added", "success");                
                this.showform = !this.showform;
                this.loaddata();
            }
            
            }, error => {})



        }   

     viewAction(id)
     {
         this.router.navigate(['userview/'+id]);
     }


loaddata()
{

	 this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_Userlist/').subscribe((data: any) => {  
 	this.spinner.hide();        

 	
        this.rowsFilter = data.data;
        console.log(this.rowsFilter)  
        this.AllData    = [];
	})

}

   
}



