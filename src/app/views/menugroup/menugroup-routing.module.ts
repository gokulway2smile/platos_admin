import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { MenugroupComponent } from './menugroup.component';

const routes: Routes = [
  { 
    path: '',
    component: MenugroupComponent,
    data: {
      title: 'Menugroup'
    },

  } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)], 
  exports: [RouterModule]
})
export class MenugroupRoutingModule { }
