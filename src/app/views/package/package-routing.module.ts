import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { PackageComponent } from './package.component'

const routes: Routes = [
  {
    path: '',
    component: PackageComponent,
    data: {
      title: 'Package'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)], 
  exports: [RouterModule]
})
export class PackageRoutingModule { }
