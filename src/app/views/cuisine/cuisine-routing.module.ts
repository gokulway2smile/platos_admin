import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { CuisineComponent } from './cuisine.component';

const routes: Routes = [
  {
    path: '',
    component: CuisineComponent,
    data: {
      title: 'Cuisine'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)], 
  exports: [RouterModule]
})
export class CuisineRoutingModule { }
