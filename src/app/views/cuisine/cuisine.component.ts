import { Component, OnInit,ViewChild,TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../app.setting';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-cuisine',
  templateUrl: './cuisine.component.html', 
  styleUrls: ['./cuisine.component.scss']
})
export class CuisineComponent implements OnInit {

  constructor(private router: Router, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) { }


  rowsFilter:any = []; 
  viewdata :any = [];
  AllData:any=[]; 
  is_view:boolean = false;
  is_edit:boolean =false;
  title :any;
  username:any;
  editdata:any;
  error:any;
  is_add:any;
  myFiles:any=[];
  ShowForm:boolean = false;

  ngOnInit() {
  	this.loaddata();
  }

  close()
  {
  	this.is_view = false;
    this.is_edit = false;
    this.is_add  = false;

  }

  edit(id)
  {
  	this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_cuisine/' +id).subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.editdata = data.data; 
        this.title = this.editdata.cuis_name

        console.log(this.editdata) 
        this.AllData    = [];
        this.is_edit = true;
        this.ShowForm = true;
	})

  }

 //    getFileDetails (e) {
 //   //console.log (e.target.files);
 //   for (var i = 0; i < e.target.files.length; i++) {
 //     this.myFiles.push(e.target.files[i]);
 //   }   
 // }

 onSubmit()
     {


     	 this.error = "";   

        
        if (!this.title) {
            this.error = "Please Enter Title";
            return;
        }
       

        if(!this.is_edit)
        {
           const frmData = new FormData();
           
           let data1;
            if (frmData)  
            {
                let temp_data:any;
                 data1 = {
                    "cuisine_name": this.title,
                }

                this.addnew(data1);
            }
            else
                {
                    data1 = {
                    "cuisine_name": this.title,
                }
                    this.addnew(data1);           
                }                
        }
        else
        {
           const frmData = new FormData();
           let data1;
           if(this.myFiles.length>0)
           {
           
                 data1 = {                    
                    "cuisine_name": this.title,
                }
                    this.updateone(data1);
                }
                else
                {
                     data1 = {
                    "cuisine_name": this.title,                
                }  
                   this.updateone(data1);
                }

        }
        }


 addnew(data1)
    {
        this.spinner.show();  
        this.http.post(AppSettings.API_ENDPOINT + 'getall_cuisine/', {
                "data": data1,
            }).subscribe((data: any) => {

        this.spinner.show();  
                if (data.status == "ok") {
                  
                  Swal("Success", "Cuisine Added", "success");
                    this.ShowForm = !this.ShowForm;
                    this.loaddata();                    
                    return;
                } else {
                Swal("Failure", "Not Added", "error");
                this.loaddata();
            }
            }, error => {})
        
    }

     updateone(data1)
    {
            this.spinner.show();  
            this.http.put(AppSettings.API_ENDPOINT + 'getall_cuisine/' + this.editdata.cuis_id, {
                "data": data1,
            }).subscribe((data: any) => {
            this.spinner.show();  
                if (data.status == "ok") {
                  
                  Swal("Success", " Cuisine Updated", "success");
                  this.ShowForm = !this.ShowForm;
                  this.loaddata();
                    

                    return;
                } else {
                Swal("Failure", " Not Updated", "error");
                this.ShowForm = !this.ShowForm;
                this.loaddata();
            }
            }, error => {})

    }

  addAction()
    {
        this.title =""
        this.ShowForm = true;
        this.is_add =true;

    }

    viewAction(id)
 {

 	this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_cuisine/' +id).subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.viewdata = data.data;
        console.log(this.viewdata)  
        this.AllData    = [];
        this.is_view = true;
        this.ShowForm = true;
	})


 }

 delete(id)
{
    this.spinner.show();        
    
    this.http.delete(AppSettings.API_ENDPOINT + 'getall_cuisine/' +id).subscribe((data: any) => {  
     this.spinner.hide();        
        
       this.loaddata();
    })

}

  loaddata()
{
  this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_cuisine').subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.rowsFilter = data.data;
        console.log(this.rowsFilter)  
        this.AllData    = [];
	})
}

} 
