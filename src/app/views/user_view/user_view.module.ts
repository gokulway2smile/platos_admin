import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { UserViewComponents } from './user_view.component';
import { UsersViewRoutingModule } from './user_view-routing.module';
import {CommonModule} from  '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    FormsModule,
    UsersViewRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),NgxDatatableModule,CommonModule
  ],
  declarations: [ UserViewComponents ]
})
export class UsersViewModule { }
