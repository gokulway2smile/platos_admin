import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { MealtypeComponent } from './mealtype.component';

const routes: Routes = [
  { 
    path: '',
    component: MealtypeComponent,
    data: {
      title: 'Mealtype'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)], 
  exports: [RouterModule]
})
export class MealtypeRoutingModule { }
