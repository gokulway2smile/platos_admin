import { Component, OnInit,ViewChild,TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../app.setting';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
 
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  constructor(private router: Router, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) { }

  rowsFilter:any = []; 
  viewdata :any = [];
  AllData:any=[]; 
  is_view:boolean = false;
  is_edit:boolean =false;
  Amount :any;
  Quantity:any;
  editdata:any;
  MenuGroup:any="";
  Menulist:any="";
  UserList:any="";
  error:any;
  is_add:any;
  myFiles:any=[];
  ShowForm:boolean = false;
  Menu_list:any=[];
  Userlist:any;
  MenuGroup_list:any=[];
  User_list:any="";

  catt_id:any;

  ngOnInit() {
  	this.loaddata();
  }

  close()
  {
  	this.is_view = false;
    this.is_edit = false;
    this.is_add  = false;

  }

  edit(id)
  {
  	this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_orders/' +id).subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.editdata = data.data; 
        this.UserList = this.editdata.catt_id
        this.MenuGroup = this.editdata.menugroup_id
        this.Menulist = this.editdata.menu_id
        this.Quantity = this.editdata.quantity
        this.Amount = this.editdata.amount

        console.log(this.editdata) 
        this.AllData    = [];
        this.is_edit = true;
        this.ShowForm = true;
	})

  }

 //    getFileDetails (e) {
 //   //console.log (e.target.files);
 //   for (var i = 0; i < e.target.files.length; i++) {
 //     this.myFiles.push(e.target.files[i]);
 //   }   
 // }

 onSubmit()
     {

       this.error = "";   

        if (!this.Quantity) {
            this.error = "Please Enter Quantity";
            return;
        }
       

        if(!this.is_edit)
        {
           
           let data1;

                    data1 = {
                    "catt_id": this.Userlist,
                 	  "menu_id": this.Menulist,
                    "quantity": this.Quantity,
                    "amount": this.Amount,
                
                    

                }   
                this.addnew(data1);    
                console.log(data1);             
        }
        else
        {
           const frmData = new FormData();
           let data1;
           if(this.myFiles.length>0)
           {
           
                 data1 = {    
                 	"catt_id": this.UserList,
                 	"menu_id": this.Menulist,                
                    "quantity": this.Quantity,
                    "amount": this.Amount,
                }
                    this.updateone(data1);
                }
                else
                {
                     data1 = {
                     "catt_id": this.UserList,
                 	"menu_id": this.Menulist,
                    "quantity": this.Quantity,
                    "amount": this.Amount,            
                }  
                   this.updateone(data1);
                }

        }
        }


 addnew(data1)
    {
        this.spinner.show();  
        this.http.post(AppSettings.API_ENDPOINT + 'getall_orders/', {
                "data": data1,
            }).subscribe((data: any) => {

        this.spinner.show();  
                if (data.status == "ok") {
                  
                  Swal("Success", "Order Added", "success");
                    this.ShowForm = !this.ShowForm;
                    this.loaddata();                    
                    return;
                } else {
                Swal("Failure", "Not Added", "error");
                this.loaddata();
            }
            }, error => {})
        
    }

     updateone(data1)
    {
            this.spinner.show();  
            this.http.put(AppSettings.API_ENDPOINT + 'getall_orders/' + this.editdata.menugroup_id, {
                "data": data1,
            }).subscribe((data: any) => {
            this.spinner.show();  
                if (data.status == "ok") {
                  
                  Swal("Success", " Order Updated", "success");
                  this.ShowForm = !this.ShowForm;
                  this.loaddata();
                    

                    return;
                } else {
                Swal("Failure", " Not Updated", "error");
                this.ShowForm = !this.ShowForm;
                this.loaddata();
            }
            }, error => {})

    }

  addAction()
    {
        this.Quantity ="";
        this.Amount ="";
        this.MenuGroup ="";
        this.Menulist ="";
        this.UserList ="";
        this.ShowForm = true;
        this.is_add =true;

    }

    viewAction(id)
 {

 	this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_orders/' +id).subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.viewdata = data.data;
        console.log(this.viewdata)  
        this.AllData    = [];
        this.is_view = true;
        this.ShowForm = true;
	})


 }

 delete(id)
{
    this.spinner.show();        
    
    this.http.delete(AppSettings.API_ENDPOINT + 'getall_orders/' +id).subscribe((data: any) => {  
     this.spinner.hide();        
        
       this.loaddata();
    })

}

  loaddata()
{
  this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_orders').subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.rowsFilter = data.data;
        console.log(this.rowsFilter)  
        this.AllData    = [];
	})
	
    this.http.get(AppSettings.API_ENDPOINT + 'getall_catterlist/').subscribe((data: any) => {
    this.spinner.hide();
       this.User_list = data.data;
       console.log(this.User_list)      
        
    })    
}



onChange() {
    if(this.Userlist)
    {
     this.Userlist = this.Userlist;
    this.http.get(AppSettings.API_ENDPOINT + 'getall_menulist/' +this.Userlist).subscribe((data: any) => {
    this.Menu_list = data.data;
    console.log(this.Menu_list);
    // Menulist

    })

}

}


}
