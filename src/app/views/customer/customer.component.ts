import { Component, OnInit,ViewChild,TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../app.setting';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  constructor(private router: Router, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) { }

   rowsFilter:any = []; 
  viewdata :any = [];
  AllData:any=[]; 
  is_view:boolean = false;
  is_edit:boolean =false;
  image :any;
  editdata:any;
  error:any;
  is_add:any;
  myFiles:any=[];
  ShowForm:boolean = false;
  cust_image:any;
  fileName:any;
  filePath:any;
  fileData:any;

   image_path =AppSettings.CUSTOMER_IMAGE;
  ngOnInit() {
  	this.loaddata();
  }

  close()
  {
  	this.is_view = false;
    this.is_edit = false;
    this.is_add  = false;

  }

  edit(id)
  {
  	this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_customer/' +id).subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.editdata = data.data; 
 
        this.cust_image = this.editdata.cust_image

        console.log(this.editdata) 
        this.AllData    = [];
        this.is_edit = true;
        this.ShowForm = true;
	})

  }


        getFileDetails (e) {
   //console.log (e.target.files);
   for (var i = 0; i < e.target.files.length; i++) {
     this.myFiles.push(e.target.files[i]);
   }   
 }
  
  

  onSubmit()
     {


     	 this.error = "";

        if(!this.is_edit)
        {
           const frmData = new FormData();
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("pic", this.myFiles[i]);
           }
           }
           let data1;
            if (frmData)  
            {
                let temp_data:any;
                this.fileName = null;
                this.filePath = null;        
                this.http.post(AppSettings.API_ENDPOINT + 'cust_image/', frmData).subscribe((temp_data: any) => {
                this.fileData = temp_data.data;
                this.fileName = this.fileData.pic;
                this.filePath = this.fileData.file_path;
                
                 data1 = {
                    
                    "cust_image"   :this.fileName
                } 

                this.addnew(data1);
                });  
            }
            else
                {
                    data1 = {
                                    
                }    
                this.addnew(data1);               
                }                
                
        }
        else
        {
           const frmData = new FormData();
           let data1;
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("pic", this.myFiles[i]);
           }
                let temp_data:any;
                this.fileName = null;
                this.filePath = null;        
                this.http.post(AppSettings.API_ENDPOINT + 'cust_image/', frmData).subscribe((temp_data: any) => {
                this.fileData = temp_data.data;
                this.fileName = this.fileData.pic;
                this.filePath = this.fileData.file_path;
                
                 data1 = {                    
                    "cust_image"   :this.fileName
                } 
                
                    this.updateone(data1);
                });  
                }
                else
                {
                     data1 = {
                  
                    "cust_image":this.cust_image                   
                }  
                   this.updateone(data1);
                }

        }


        } 
       
       addnew(data1)
    {
        this.spinner.show();  
        this.http.post(AppSettings.API_ENDPOINT + 'getall_customer/', {
                "data": data1,
            }).subscribe((data: any) => {
        this.spinner.show();  
                if (data.status == "ok") {
                  Swal("Success", "Image Added", "success");
                  this.ShowForm = !this.ShowForm;
                  this.loaddata();
                    
                    return;
                } else {
                Swal("Failure", "Not Added", "error");
                this.loaddata();
                // this.ShowForm = !this.ShowForm;
                this.loaddata();
            }
            }, error => {})
        
    }


      updateone(data1)
    {
            this.spinner.show();  
            this.http.put(AppSettings.API_ENDPOINT + 'getall_customer/' + this.editdata.custimg_id, {
                "data": data1,
            }).subscribe((data: any) => {
            this.spinner.show();  
                if (data.status == "ok") {
                  Swal("Success", " Image Updated", "success");
                  this.ShowForm = !this.ShowForm;
                    this.loaddata();

                    return;
                } else {
                Swal("Failure", " Not Updated", "error");
                this.ShowForm = !this.ShowForm;
                this.loaddata();
            }
            }, error => {})

    }

    addAction()
    {
        this.image=""
        this.is_add =true;
        this.ShowForm = true;

    }

delete(id)
{
    this.spinner.show();        
    
    this.http.delete(AppSettings.API_ENDPOINT + 'getall_customer/' +id).subscribe((data: any) => {  
     this.spinner.hide();        
        
       this.loaddata();
    })

}

 viewAction(id)
 {

 	this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_customer/' +id).subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.viewdata = data.data;
        console.log(this.viewdata)  
        this.AllData    = [];
        this.is_view = true;
        this.ShowForm = true;
	})


 }

loaddata()
{
  this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_customer/').subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.rowsFilter = data.data;
        console.log(this.rowsFilter)  
        this.AllData    = [];
	})
}

}
