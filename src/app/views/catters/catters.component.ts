import {
    Component,
    OnInit,
    ViewChild,
    TemplateRef
} from '@angular/core';
import {
    FormControl,
    FormGroup, 
    FormBuilder,
    Validators
} from '@angular/forms';
import { 
    HttpClient
} from '@angular/common/http';

import {
    ActivatedRoute,
    Router
} from "@angular/router";
import {
    DatatableComponent
} from '@swimlane/ngx-datatable';
import {
    AppSettings
} from '../../app.setting';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-catters',
  templateUrl: './catters.component.html',
})
  


export class CattersComponents implements OnInit { 

 constructor( private router: Router, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) {
    }

    rowsFilter:any;
    Event_list:string [];
    AllData:any;
    eventData:any;
    FirstName:any;
    SurName:any;
    EmailAddress:any;
    MobileNo:any;
    Password:any;
    // Address:any;
    // DeliveryPincode:any;
    LeadTime:any;
    DeliveryTime_from:any;
    DeliveryTime_to:any
    error:any;
    myFiles:any=[];
    myFiles1:any=[];
    myFiles2:any=[];
    MinOrder:any;
    maxOrder:any;
    EventType:any;
    GSTCertificate:any;
    FoodCertificate:any;
    Scores:any;
    meridian: boolean = true;
    is_edit:boolean = false;
    FileUpload:any;
    editdata:any;
    is_add:any;
    is_view:boolean = false;
    ShowForm:boolean = false;
    viewdata :any = [];
    fileName :any = [];
    fileName1 :any = [];
    fileName2 :any = [];
    filePath:any;
    fileData:any;
    filePath1:any;
    fileData1:any;
    filePath2:any;
    fileData2:any;
     image = [{
       GST:"",
       Food:"",
       upload:""
     }];
      
     selected = null;
    Address = [{
       address:"",
       pincode:"",
     }];

    newAttribute: any = {};

    fieldArray: Array<any> = [];

  firstField = true;
  // firstFieldName = 'First Item name';
  isEditItems: boolean;

    image_path =AppSettings.GST_IMAGE;
    image_path1 =AppSettings.FOOD_IMAGE;
    image_path2 =AppSettings.FILE_IMAGE;

     ngOnInit() {
       this.loaddata();

     }

     close()
     {
       this.is_add  = false;
       this.is_view  = false;
       this.is_edit  = false;
     }


 edit(id)
  {
      this.spinner.show();        
        
    this.http.get(AppSettings.API_ENDPOINT + 'getall_catterlist/' +id).subscribe((data: any) => {  
     this.spinner.hide();            
     
        this.editdata = data.data; 

        this.FirstName = this.editdata.catt_first_name
        
        this.SurName = this.editdata.catt_sur_name
        this.EmailAddress = this.editdata.catt_email_address
        this.MobileNo = this.editdata.catt_mobile_no
        this.Password = this.editdata.catt_password
        // this.DeliveryPincode = this.editdata.catt_delivery_pincode
        this.DeliveryTime_from = this.editdata.catt_delivery_time
        this.DeliveryTime_to = this.editdata.catt_delivery_time
        this.MinOrder = this.editdata.catt_min_order
        this.maxOrder = this.editdata.catt_max_order
        this.LeadTime = this.editdata.catt_lead_time
        this.EventType = this.editdata.catt_event_type
        this.GSTCertificate = this.editdata.catt_GST_certificate
        this.FoodCertificate = this.editdata.catt_food_certificate
        this.Scores = this.editdata.catt_score
        this.FileUpload = this.editdata.catt_file_upload

        console.log(this.editdata) 
        this.AllData    = [];
        this.is_edit = true;
        this.ShowForm = true;
        
    })

  }

 getFileDetails (e) {
   //console.log (e.target.files);
   for (var i = 0; i < e.target.files.length; i++) {
     this.myFiles.push(e.target.files[i]);
   }   
 }

 getFileDetails1 (e) {
   //console.log (e.target.files);
   for (var i = 0; i < e.target.files.length; i++) {
     this.myFiles1.push(e.target.files[i]);
   }   
 }

 getFileDetails2 (e) {
   //console.log (e.target.files);
   for (var i = 0; i < e.target.files.length; i++) {
     this.myFiles2.push(e.target.files[i]);
   }   
 }
  
  


     addAction()
     {
       
        this.FirstName="";
        this.SurName="";
        this.EmailAddress="";
        this.MobileNo="";
        this.Password="";
        // this.DeliveryPincode="";
        this.LeadTime="";
        this.DeliveryTime_from="";
        this.DeliveryTime_to="";
        this.MinOrder="";
        this.maxOrder="";
        this.EventType="";
        this.GSTCertificate="";
        this.FoodCertificate="";
        this.Scores="";
        this.FileUpload="";
        this.is_add =true;
        this.ShowForm = true;
        

     }

     onSubmit()
     {
        

         this.error = "";          
        // if (!this.FirstName) {
        //     this.error = "Please Enter FirstName";
        //     return;
        // }
        // if (!this.SurName) {
        //     this.error = "Please Enter SurName";
        //     return;
        // }
        // if (!this.MobileNo) {
        //     this.error = "Please Enter MobileNo";
        //     return;
        // }
        // if (!this.Password) {
        //     this.error = "Please Enter Password";
        //     return;
        // }
  
        // if (!this.LeadTime) {
        //     this.error = "Please Enter Lead Time";
        //     return;
        // }
        // if (!this.DeliveryTime_from) {
        //     this.error = "Please Enter Delivery From Time";
        //     return;
        // }
        // if (!this.DeliveryTime_to) {
        //     this.error = "Please Enter Delivery To Time";
        //     return;
        // }

        if(!this.is_edit)
        {
           const frmData = new FormData();
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("gstpic", this.myFiles[i]);
           }
           }

            const frmData1 = new FormData();
             if(this.myFiles1.length>0)
               {

               for (var i = 0; i < this.myFiles1.length; i++) {
                 frmData1.append("foodpic", this.myFiles1[i]);
               }
               }

               const frmData2 = new FormData();
                if(this.myFiles2.length>0)
               {
               for (var i = 0; i < this.myFiles2.length; i++) {
                 frmData2.append("filepic", this.myFiles2[i]);
               }
               }

           let data1;
            // if (frmData && frmData1 && frmData2)  
            // {
                 if(this.myFiles.length>0)
               {
                   let temp_data:any;
                   this.fileName = [];
                   this.filePath = null;
                   this.http.post(AppSettings.API_ENDPOINT + 'catgst_image/', frmData).subscribe((temp_data: any) => {
                  
                     if(temp_data)
                   {
                       this.fileData = temp_data.data;
                       this.fileName.push(this.fileData);
                   }
                   else
                   {

                    this.fileName =[];
                   }

                   console.log(this.fileName)

                   // this.fileData = temp_data.data;
                   // this.fileName = this.fileData.gstpic;
                   // this.filePath = this.fileData.file_path;


                   

                  

                   // this.image.push({'GST':this.fileName});

                   });
               }

               if(this.myFiles1.length>0)
               {
              let temp_data1:any;
               this.fileName1 = [];
               this.filePath1 = null;
               this.http.post(AppSettings.API_ENDPOINT + 'catfood_image/', frmData1).subscribe((temp_data1: any) => {
               
                  if(temp_data1)
                   {
                       this.fileData1 = temp_data1.data;
                       this.fileName1.push(this.fileData1);
                   }
                   else
                   {

                    this.fileName1 =[];
                   }

                   console.log(this.fileName1)

               // this.fileData1 = temp_data1.data;
               // this.fileName1 = this.fileData1.foodpic;
               // this.filePath1 = this.fileData1.file_path;

               

               // this.image.push({'Food':this.fileName1});
                   });
               }

                  if(this.myFiles2.length>0)
               {

              let temp_data2:any;
               this.fileName2 = [];
               this.filePath2 = null;
               this.http.post(AppSettings.API_ENDPOINT + 'catfile_image/', frmData2).subscribe((temp_data2: any) => {
               

                 if(temp_data2)
                   {
                       this.fileData2 = temp_data2.data;
                       this.fileName2.push(this.fileData2);
                   }
                   else
                   {

                    this.fileName2 =[];
                   }

                   console.log(this.fileName2)

               // this.fileData2 = temp_data2.data;
               // this.fileName2 = this.fileData2.filepic;
               // this.filePath2 = this.fileData2.file_path;

               

               // this.image.push({'upload':this.fileName2});

              });

               }

               let fromtime= this.DeliveryTime_from.hour + ":" + this.DeliveryTime_from.minute;
               let totime= this.DeliveryTime_to.hour + ":" + this.DeliveryTime_to.minute;

               // console.log(this.image);
               // let imagearray = this.image;

                 data1 = {
                    "catt_first_name": this.FirstName,
                    "catt_sur_name": this.SurName,
                    "catt_email_address": this.EmailAddress,
                    "catt_mobile_no": this.MobileNo,
                    "catt_password": this.Password,
                    "catt_lead_time": this.LeadTime,
                    "catt_address" : this.Address,
                    "DeliveryTime_from": fromtime,
                    "DeliveryTime_to": totime,
                    "catt_min_order" : this.MinOrder,
                    "catt_max_order" : this.maxOrder,
                    "catt_event_type" : this.EventType,
                    "catt_GST_certificate" : this.fileName,
                    "catt_food_certificate" :this.fileName1,
                    // "catt_files" : this.image,
                    "catt_score" :this.Scores,
                    "catt_file_upload":this.fileName2
                    
                    
                }

                // console.log(newimage);
                console.log(data1);
                this.addnew(data1);
                }

                // this.addnew(data1);
                
            
            // else
            //     {
            //         data1 = {
            //         "catt_first_name": this.FirstName,
            //         "catt_sur_name": this.SurName,
            //         "catt_email_address": this.EmailAddress,
            //         "catt_mobile_no": this.MobileNo,
            //         "catt_password": this.Password,
            //         "catt_address" : this.Address,
            //         "catt_lead_time": this.LeadTime,
            //         "DeliveryTime_from": this.DeliveryTime_from,
            //         "DeliveryTime_to": this.DeliveryTime_to,
            //         "catt_min_order" : this.MinOrder,
            //         "catt_max_order" : this.maxOrder,
            //         "catt_event_type" : this.EventType,
            //         // "catt_GST_certificate" : this.GSTCertificate,
            //         // "catt_food_certificate" :this.FoodCertificate,
            //         "catt_score" :this.Scores,
            //         // "catt_file_upload":this.FileUpload
                                    
            //     }
            //     this.addnew(data1);                   
            //     }                
                
        // }
        else
        {
           const frmData = new FormData();
           let data1;
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("pic", this.myFiles[i]);
           }
                let temp_data:any;
                this.fileName = null;
                this.filePath = null;        
                this.http.post(AppSettings.API_ENDPOINT + 'catgst_image/', frmData).subscribe((temp_data: any) => {
                this.fileData = temp_data.data;
                this.fileName = this.fileData.pic;
                this.filePath = this.fileData.file_path;
                

              if(this.myFiles1.length>0)
               {
               for (var i = 0; i < this.myFiles1.length; i++) {
                 frmData.append("foodpic", this.myFiles1[i]);
               }
               }

               let temp_data1:any;
                this.fileName1 = null;
                this.filePath1 = null;        
                this.http.post(AppSettings.API_ENDPOINT + 'catfood_image/', frmData).subscribe((temp_data1: any) => {
                this.fileData1 = temp_data1.data;
                this.fileName1 = this.fileData1.foodpic;
                this.filePath1 = this.fileData1.file_path;


                if(this.myFiles2.length>0)
               {
               for (var i = 0; i < this.myFiles2.length; i++) {
                 frmData.append("filepic", this.myFiles2[i]);
               }
               }

               let temp_data2:any;
                this.fileName2 = null;
                this.filePath2 = null;        
                this.http.post(AppSettings.API_ENDPOINT + 'catfile_image/', frmData).subscribe((temp_data2: any) => {
                this.fileData2 = temp_data2.data;
                this.fileName2 = this.fileData2.filepic;
                this.filePath2 = this.fileData2.file_path;


                 data1 = {                    
                   "catt_first_name": this.FirstName,
                    "catt_sur_name": this.SurName,
                    "catt_email_address": this.EmailAddress,
                    "catt_mobile_no": this.MobileNo,
                    "catt_password": this.Password,
                    "catt_address" : this.Address,
                    // "catt_delivery_pincode": this.DeliveryPincode,
                    "catt_lead_time": this.LeadTime,
                    "DeliveryTime_from": this.DeliveryTime_from,
                    "DeliveryTime_to": this.DeliveryTime_to,
                    "catt_min_order" : this.MinOrder,
                    "catt_max_order" : this.maxOrder,
                    "catt_event_type" : this.EventType,
                    "catt_GST_certificate" : this.fileName,
                    "catt_food_certificate" :this.fileName1,
                    "catt_score" :this.Scores,
                    "catt_file_upload":this.fileName2
                    
                } 
                
                    this.updateone(data1);
                 });
                 });
                 });

                }
                else
                {
                     data1 = {
                    "catt_first_name": this.FirstName,
                    "catt_sur_name": this.SurName,
                    "catt_email_address": this.EmailAddress,
                    "catt_mobile_no": this.MobileNo,
                    "catt_password": this.Password,
                    "catt_address" : this.Address,
                    // "catt_delivery_pincode": this.DeliveryPincode,
                    "catt_lead_time": this.LeadTime,
                    "DeliveryTime_from": this.DeliveryTime_from,
                    "DeliveryTime_to": this.DeliveryTime_to,
                    "catt_min_order" : this.MinOrder,
                    "catt_max_order" : this.maxOrder,
                    "catt_event_type" : this.EventType,
                    "catt_GST_certificate" : this.GSTCertificate,
                    "catt_food_certificate" :this.FoodCertificate,
                    "catt_score" :this.Scores,
                    "catt_file_upload":this.FileUpload
                    
                                   
                }  
                   this.updateone(data1);
                }

        }



        }

        addnew(data1)
    {
        // console.log(newimage[0]);

        this.spinner.show();  
        this.http.post(AppSettings.API_ENDPOINT + 'getall_catterlist/', {
                "data": data1,
            }).subscribe((data: any) => {
              console.log(data);
        this.spinner.show();  
                if (data.status == "ok") {
                    Swal("Success", "Caterer Added", "success");
                    this.ShowForm = !this.ShowForm;
                    this.loaddata();
                    
                    return;
                } else {
                Swal("Failure", "Not Added", "error");
                this.ShowForm = !this.ShowForm;
                this.loaddata();
            }
            }, error => {})
        
    }

       updateone(data1)
    {
            this.spinner.show();  
            this.http.put(AppSettings.API_ENDPOINT + 'getall_catterlist/' + this.editdata.catt_id, {
                "data": data1,
            }).subscribe((data: any) => {
            this.spinner.show();  
                if (data.status == "ok") {
                    Swal("Success", " Caterer Updated", "success");
                    this.ShowForm = !this.ShowForm;
                    this.loaddata();
                    return;
                } else {
                Swal("Failure", " Not Updated", "error");
                this.ShowForm = !this.ShowForm;
                this.loaddata();
            }
            }, error => {})

    }

delete(id)
{
    this.spinner.show();        
    
    this.http.delete(AppSettings.API_ENDPOINT + 'getall_catterlist/' +id).subscribe((data: any) => {  
     this.spinner.hide();        
        
       this.loaddata();
    })

}

    viewAction(id)
 {

     this.spinner.show();        
    
    this.http.get(AppSettings.API_ENDPOINT + 'getall_catterlist/' +id).subscribe((data: any) => {  
     this.spinner.hide();        
     
        this.viewdata = data.data[0];
        console.log(this.viewdata)  
        this.AllData    = [];
        this.is_view = true;
        this.ShowForm = true;
    })


 }   

     
loaddata()
{

   this.spinner.show();        
  
  this.http.get(AppSettings.API_ENDPOINT + 'getall_catterlist/').subscribe((data: any) => {  
   this.spinner.hide();        

   
        this.rowsFilter = data.data;
        console.log(this.rowsFilter)  
        this.AllData    = [];
  })

   this.http.get(AppSettings.API_ENDPOINT + 'getall_events/').subscribe((data: any) => {
    this.spinner.hide();

       this.Event_list = data.data;
        
    })

}

addItem()
   {
     this.Address.push({
       address:this.Address[0].address,
       pincode :this.Address[0].pincode
     })
   }

// addFieldValue(index) {
//     if (this.fieldArray.length <= 4) {
//       this.fieldArray.push(this.newAttribute);
//       this.newAttribute = {};
//     } else {

//     }
//   }

  deleteFieldValue(index) {
    this.Address.splice(index, 1);
  }

   
}



