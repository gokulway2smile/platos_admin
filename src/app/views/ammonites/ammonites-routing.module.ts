import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { AmmonitesComponent } from './ammonites.component'

const routes: Routes = [
  {
    path: '',
    component: AmmonitesComponent,
    data: {
      title: 'Ammonites'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)], 
  exports: [RouterModule]
})
export class AmmonitesRoutingModule { }
