import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmmonitesComponent } from './ammonites.component';

describe('AmmonitesComponent', () => {
  let component: AmmonitesComponent;
  let fixture: ComponentFixture<AmmonitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmmonitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmmonitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
