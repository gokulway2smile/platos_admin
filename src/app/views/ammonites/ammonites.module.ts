import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { AmmonitesComponent } from './ammonites.component';
import { AmmonitesRoutingModule} from './ammonites-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [AmmonitesComponent],
  imports: [
    CommonModule,
    FormsModule,
    AmmonitesRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgxDatatableModule
  ]
})
export class AmmonitesModule { }
