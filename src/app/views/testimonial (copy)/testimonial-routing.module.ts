import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { TestimonialComponent } from './testimonial.component';

const routes: Routes = [
  {
    path: '',
    component: TestimonialComponent,
    data: {
      title: 'Testimonial'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestimonialRoutingModule {}
